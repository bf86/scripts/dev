package main

import (
	"log"
	"os"
	"time"

	"github.com/urfave/cli/v2"
	"gitlab.com/bf86/scripts/toolbox/services"
)

var version string

func main() {
	cli.VersionFlag = &cli.BoolFlag{
		Name:    "version",
		Aliases: []string{"v"},
		Usage:   "display the current tool version",
	}

	app := cli.NewApp()
	app.Name = "cli"
	app.Version = version
	app.Compiled = time.Now()
	app.Usage = "development tools"
	app.Authors = []*cli.Author{
		{
			Name:  "James King",
			Email: "jameshdking@gmail.com",
		},
	}

	addService(services.NewDockerService(app))
	addService(services.NewDockerComposeService(app))

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func addService(service services.ServiceIface, err error) {
	if service == nil {
		log.Printf("%s. skipping...", err)
		return
	}

	service.App().Commands = append(service.App().Commands, service.Commands()...)
}

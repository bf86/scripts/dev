package services

import (
	"log"
	"os"
	"os/exec"

	"github.com/urfave/cli/v2"
)

type Service struct {
	app  *cli.App
	cmd  string
	args []string
}

type ServiceIface interface {
	App() *cli.App
	Args() []string
	Command() string
	Commands() []*cli.Command
}

func NewService(app *cli.App, cmd string) *Service {
	return &Service{app: app, cmd: cmd}
}

func sendCommand(service ServiceIface) error {
	cmd := exec.Command(service.Command(), service.Args()...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	log.Printf("### %v ###", cmd.String())

	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}

func commandExists(command string) bool {
	_, err := exec.LookPath(command)
	return err == nil
}

func (s *Service) App() *cli.App            { return s.app }
func (s *Service) Args() []string           { return s.args }
func (s *Service) Command() string          { return s.cmd }
func (c *Service) Commands() []*cli.Command { return []*cli.Command{} }
